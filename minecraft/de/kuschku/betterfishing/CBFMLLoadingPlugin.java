package de.kuschku.betterfishing;

import java.io.File;
import java.util.Map;

import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin;

@NetworkMod(clientSideRequired=true)
public class CBFMLLoadingPlugin implements IFMLLoadingPlugin {

	public static File location;

	@Override
	@Deprecated
	public String[] getLibraryRequestClass() {
		return null;
	}

	@Override
	public String[] getASMTransformerClass() {
		return new String[] { CBClassTransformer.class.getName() };
	}

	@Override
	public String getModContainerClass() {
		return CBDummyContainer.class.getName();
	}

	@Override
	public String getSetupClass() {
		return null;
	}

	@Override
	public void injectData(Map<String, Object> data) {
		location = (File) data.get("coremodLocation");
	}

}
