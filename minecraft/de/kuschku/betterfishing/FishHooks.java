package de.kuschku.betterfishing;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import cpw.mods.fml.common.SidedProxy;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFishingRod;
import net.minecraft.item.ItemStack;

public class FishHooks {	
	static final Random rand = new Random();

	public static Map<ItemStack, Float> treasure = new HashMap<ItemStack, Float>();
	public static Map<ItemStack, Float> junk = new HashMap<ItemStack, Float>();
	public static Map<ItemStack, Float> fish = new HashMap<ItemStack, Float>();

	public static ItemStack getItem(ItemStack currentItem) {
		while (true) {
			Map<ItemStack, Float> set;
			float type = rand.nextFloat();
			if (type <= 0.05f) {
				set = treasure;
			} else if (type <= 0.25f) {
				set = junk;
			} else {
				set = fish;
			}

			float randomValue = rand.nextFloat();
			float cumulativeProbability = 0.0f;

			for (Entry<ItemStack, Float> item : set.entrySet()) {
				cumulativeProbability += item.getValue();
				if (randomValue <= cumulativeProbability) {
					ItemStack itemStack = item.getKey();
					if (itemStack.getItem()==Item.bow) {
						itemStack.setItemDamage(rand.nextInt(Item.bow
								.getMaxDamage()));
						itemStack = Items.addRandomEnchantment(itemStack, Enchantment.flame, true);
						itemStack = Items.addRandomEnchantment(itemStack, Enchantment.power, true);
						itemStack = Items.addRandomEnchantment(itemStack, Enchantment.punch, true);
						itemStack = Items.addRandomEnchantment(itemStack, Enchantment.infinity, true);
					} else if (itemStack.getItem()==Item.enchantedBook) {
						Enchantment[] list = Enchantment.enchantmentsBookList;
						Enchantment enchantment = list[rand.nextInt(list.length)];
						itemStack = Items.addRandomEnchantment(itemStack, enchantment, true);
					} else if (itemStack.getItem()==Item.bootsLeather) {
						itemStack.setItemDamage(rand.nextInt(Item.bootsLeather
								.getMaxDamage()));
					} else if (itemStack.getItem()==Item.fishingRod) {
						itemStack.setItemDamage(rand.nextInt(Item.fishingRod
								.getMaxDamage()));
					}
					return item.getKey();
				}
			}
		}
	}
}
