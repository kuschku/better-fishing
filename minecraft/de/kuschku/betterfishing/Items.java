package de.kuschku.betterfishing;

import java.util.Map.Entry;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

public class Items {

	private final static Random rand = new Random();

	public static void populateItems() {
		FishHooks.fish.put(new ItemStack(Item.fishRaw), 1.0f);

		FishHooks.treasure.put(new ItemStack(Item.bow, 1), 0.166666666667f);
		FishHooks.treasure.put(new ItemStack(Item.enchantedBook), 0.166666666667f);
		FishHooks.treasure.put(new ItemStack(Item.fishingRod), 0.166666666667f);
		FishHooks.treasure.put(new ItemStack(Item.nameTag), 0.166666666667f);
		FishHooks.treasure.put(new ItemStack(Item.saddle), 0.166666666667f);
		FishHooks.treasure.put(new ItemStack(Block.waterlily), 0.166666666667f);

		FishHooks.junk.put(new ItemStack(Item.bowlEmpty), 0.120481927711f);
		FishHooks.junk.put(new ItemStack(Item.leather), 0.120481927711f);
		FishHooks.junk
				.put(new ItemStack(Item.bootsLeather, 1), 0.120481927711f);
		FishHooks.junk.put(new ItemStack(Item.rottenFlesh), 0.120481927711f);
		FishHooks.junk.put(new ItemStack(Item.potion, 1, 0), 0.120481927711f);
		FishHooks.junk.put(new ItemStack(Item.bone), 0.120481927711f);
		FishHooks.junk
				.put(new ItemStack(Block.tripWireSource), 0.120481927711f);
		FishHooks.junk.put(new ItemStack(Item.stick), 0.0602409638554f);
		FishHooks.junk.put(new ItemStack(Item.silk), 0.0602409638554f);
		FishHooks.junk.put(new ItemStack(Item.fishingRod, 1), 0.0240963855422f);
		FishHooks.junk.put(new ItemStack(Item.dyePowder, 10, 0),
				0.0120481927711f);
	}

	public static ItemStack addRandomEnchantment(ItemStack item,
			Enchantment enchantment, boolean always) {
		if (enchantment.getMaxLevel() != enchantment.getMinLevel()) {
			byte enchantment_level;
			if (!always) {
				enchantment_level = (byte) rand.nextInt(enchantment
						.getMaxLevel());
			} else {
				enchantment_level = (byte) (rand.nextInt(enchantment
						.getMaxLevel() - enchantment.getMinLevel()) + enchantment
						.getMinLevel());
			}
			if (enchantment_level > enchantment.getMinLevel()) {
				item.addEnchantment(enchantment, enchantment_level);
			}
		} else {
			if (always || rand.nextFloat() > 0.5)
				item.addEnchantment(enchantment, 1);
		}
		return item;
	}
}
